# Gimme the Points

A node application to automate the actions needed to receive microsoft rewards points using Selenium WebDriver.

## Getting Started

### Prerequisites

* You will need to have NodeJS & npm installed. NodeJS preferably v12.
* You will also need to have google chrome installed.


### Installing

* Clone the repo or download and extract it. 
* Run ```npm install``` in the application folder. 

### Configuring

**Check formatting carfully**
* **Place your own accounts in ```/config/accounts.json.```** 
* Set useragents in ```/config/useragents.json```
* Number of options to set in ```/config/config.json```
  * headless window resolution
  * mobile window resolution
  * geos to get search queries from
  * Number of times to run dashboard & search

### Executing program

* **```npm start```** Will start the application with default settings(not headless, all accounts, search, dashboard and mobile)

There are also a number of parameters that can be given for using in your own script.
**Parameters in bold are required.**
* **```--runtype=```**
  * ```all``` Run all accounts in accounts.json.
  * ```single``` Run a single account from accounts.json. Must also give ```account=``` parameter.
  * ```select``` Start running from a given point in accounts.json. Must also give ```account=``` parameter.
  * ```dashboard``` Run only dashboard on a single account. Must also give ```account=``` parameter.
  * ```mobile``` Run only mobile searches on a single account. Must also give ```account=``` parameter.
  * ```emailonly``` Run only email links from reddit on a single account. Must also give ```account=``` parameter
* ```--account=```***number*** position of account in accounts.json (starts from 0).
* ```--emaillinks``` Will get rewards email links from reddit (past 31 days) and open them. 
* ```--shuffle``` Randomise the order in which accounts are ran.
* ```--extrasearches``` Will generate a number (0-9) of extra searches to run in addition.
* ```--sweepstakes``` Will enter sweepstakes offers found on the dashboard.
* ```--headless``` Will run in headless mode.
* ```--disableimgs``` Prevents the browser from loading images.
* ```--ignoreupdates``` Stops the application from checking for updates when ran.
* ```--autoupdate``` Will pull new files when remote version change detected **without** user prompt. This will force a process exit after updating.
* ```--schedule```  Will run at the hour and minute specified ```(shedule=9h23m)``` would run at 9:23am every day as long as the process remains open. 


## Help

Any advise for common problems or issues.
