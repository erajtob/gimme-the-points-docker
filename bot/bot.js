const {auth} = require('./auth')
const Driver = require('./driver')
const {withDelay, takeScreenshot, setWindowRes} = require('./helpers')
const {runOffers} = require('./dashboard')
const {searchPoints} = require('./search_points')
const pointsURL = 'http%3A%2F%2Fwww.bing.com%2Frewardsapp%2Fbepflyoutpage%3Fstyle%3Dedgeextension'
const {runEmailLinks} = require('./email_links')
const config = require('../config/config.json')

class Bot {
  constructor(driverType, headless)
  { 
    this.driver = new Driver().createDriver(driverType, headless);
  }
}

Bot.prototype.auth = async function (account) {
  await auth(account)
}

Bot.prototype.runDashboard = async function (headless) {
  if (headless) await setWindowRes(config.headlessResWidth, config.headlessResHeight)
  await runOffers()
}

Bot.prototype.pcRun = async function (stats, emaillinks, headless) { 
  if (headless) await setWindowRes(config.headlessResWidth, config.headlessResHeight)
  await searchPoints(stats, 'pc')
  await withDelay(2500)
  await runOffers()
  await withDelay(2500)
  if (emaillinks) await runEmailLinks()
}

Bot.prototype.mobileRun = async function (stats) {
  await setWindowRes(config.mobileResWidth, config.mobileResHeight)
  await searchPoints(stats, 'mobile')
}


module.exports = Bot