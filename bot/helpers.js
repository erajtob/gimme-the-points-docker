const config = require('../config/config.json')
const fs = require('fs')
const util = require('util')
const {By} = require('selenium-webdriver')


const randSeconds = () => {
  return Math.random() * (config.maxDelay - config.minDelay) + config.minDelay
}

const randBetween = (a, b) => {
  return Math.floor(Math.random() * b) + a
}

const daysToMillis = days => {
  return days * 24 * 60 * 60 * 1000
}

const formatDate = date => {
  const pad = num => num.toString().padStart(2, '0')
  return `${date.getFullYear()}${pad(date.getMonth() + 1)}${pad(date.getDate())}`
}

const trendDate = function () {
  const date = new Date(Date.now() - daysToMillis(randBetween(1, 28)))
  return formatDate(date)
}

//delay with a promise
const withDelay = (time) => {
  return new Promise(resolve => {
    setTimeout(() => {
      return resolve()
    }, time)
  }).catch(error =>{
    console.log(error)
  })
}

const tryTimes = async (count, predicate) => {
  console.log(count)
  if (predicate() || count < 1) {
    return
  } else {
    await withDelay(1000)
    return tryTimes(count - 1, predicate)
  }
}

const elementToExist = async function (finder, time) {
  if (!time) time = config.elementToExist
  return driver.wait(async function ()  {
    let element
      try {
        element = await this.driver.findElement(finder)
      } catch(e) {
      }
      return element
  }, time)
}

const elementIsDisplayed = async function (finder, time) {
  if (!time) time = config.elementIsDisplayed
  return driver.wait(async function () {
    let element
      try {
        element = await this.driver.findElement(finder).isDisplayed()
      } catch (error) {
      }
    return element
  }, time)
}

const elementIsEnabled = async function (finder, time) {
  if (!time) time = config.elementIsDisplayed
  return driver.wait(async function () {
    let element
      try {
        element = await this.driver.findElement(finder).isEnabled()
      } catch (error) {
      }
    return element
  }, time)
}

const getTab = async function (num) {
  await withDelay(500)
  const tabs = await driver.getAllWindowHandles()
  await driver.switchTo().window(tabs[num])
  await withDelay(500)
}

const getNewestTab = async function () {
  await withDelay(500)
  const tabs = await driver.getAllWindowHandles()
  await driver.switchTo().window(tabs[tabs.length - 1])
  await withDelay(500)
}

const getTabsLength = async function () {
  const tabs = await driver.getAllWindowHandles()
  return tabs.length
}

const closeTab = async function () {
  await withDelay(500)
  await driver.executeScript("window.close()")
  await getNewestTab()
  await withDelay(500)
}

const forceClick = async function (element) {
  console.log('force clicking')
  try {
    await driver.executeScript("document.querySelector('"+ element + "').click()")
  } catch (error){
    console.log(error.name)
  }
}

const setHeadless = function (args) {
  let headless
  if (args.headless == 'false' || args.headless == undefined) headless = false
  if (args.headless == 'true') headless = true
  return headless
}

const setAccounts = function () {
  let accounts
  try {accounts = require('../config/myaccounts.json')} catch {
    try {accounts = require('../config/accounts.json')} catch (error) {
    }
  };
  return accounts
}

const takeScreenshot = async function (email, name){
  const stamp = new Date().toJSON().slice(0,10)
  const writeFile = util.promisify(fs.writeFile)
  if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
  await writeFile(`./screenshots/${stamp}-${email}-${name}.png`, await driver.takeScreenshot(), 'base64')
}


const addRecoveryInfo = async function () {
  let protectForm
  try {
    protectForm = await elementToExist(By.id('frmAddProof'), 2500)
    if (protectForm) throw new Error('Please add recovery information to this account. Cannot proceed with reward tasks!') 
  } catch (e) {
    if (e.name === 'TimeoutError') return
    throw new Error('addRecoveryInfo error: '+ e)
  }
}

const ensureUrl = async function (url, regex) {
  let currentURL = await driver.getCurrentUrl(), tryTimes = 0
  while (new RegExp(regex).test(currentURL) == false) {
    await addRecoveryInfo()
    //console.log(`CURRENT URL IS: ${currentURL}`)
    if (tryTimes > 2) throw new Error(`Unable to get to ${url}.`)
    await withDelay(3000)
    //console.log(`ATTEMPTING TO REACH ${url}`)
    await driver.get(url)
    currentURL = await driver.getCurrentUrl()
    await withDelay(3000)
    
    tryTimes++
    //console.log(`Attempt ${tryTimes} done. Current url is :${currentURL} url we want to reach is ${url}`)
    //console.log(`Regex check current url ${new RegExp(regex).test(currentURL)}`)
  }
}

const ensureSignedInOld = async function (url) {
  const signInBtn = await driver.findElements(By.id('id_s'))
  if (signInBtn.length) {
    if (await signInBtn[0].isDisplayed()) {
      await driver.get('https://www.bing.com/rewards/signin')
      const links = await driver.findElements(By.tagName('a'))
      links.forEach(async link => {
        const href = await link.getAttribute('href')
        if (/Token=1/.test(href))  {
          await driver.get(href)
          if (url) await driver.get(url)
          await withDelay(randSeconds())
        }
      });
    }
  }
}

const ensureSignedIn = async function (url) {
  const signInBtn = await driver.findElements(By.id('id_a'))
  if (signInBtn.length) {
    if (await signInBtn[0].isDisplayed()) {
      await signInBtn[0].click()
    }
  }
}

function  userAgentFormatter (string) {
  if (!/^user-agent=/.test(string)) return 'user-agent='.concat(string)
  return string
}

function newArray (array, startPoint) {
  const newArray = []
  for (let index = startPoint; index < array.length; index++) {
    newArray.push(array[index])
  }
  return newArray
}

function shuffleArray (array) {
  for (let index = 0; index < array.length; index++) {
      const item = Math.floor(Math.random() * (index + 1));
      [array[index], array[item]] = [array[item], array[index]];
    }
  return array
}

function shuffleAccounts (accounts, startPoint) {
  return shuffleArray(newArray(accounts, startPoint))
}

const setWindowRes = async function (width, height) {
  await driver.manage().window().setRect({width:width, height:height})
}

async function acceptTerms (identifier) {
  const acceptButton = await driver.findElements(identifier)
  if (acceptButton.length) {
    await acceptButton[0].click()}
}

async function looksGoodInfo () { 
  const looksGoodButton = await driver.findElements(By.id('iLooksGood'))
  if (looksGoodButton.length) {
    await looksGoodButton[0].click()
  }
}

async function removeElement (id) {
  driver.executeScript(`
    let element = document.getElementById('${id}')
    element.remove
  `)
  .catch(err=>{})
}

module.exports = {
  randSeconds,
  withDelay,
  tryTimes,
  elementToExist,
  elementIsDisplayed,
  getTab,
  elementIsEnabled,
  trendDate,
  forceClick,
  setHeadless,
  setAccounts,
  takeScreenshot,
  closeTab,
  ensureUrl,
  ensureSignedIn,
  shuffleAccounts,
  setWindowRes,
  getNewestTab,
  getTabsLength,
  acceptTerms,
  looksGoodInfo,
  userAgentFormatter,
  removeElement
}
