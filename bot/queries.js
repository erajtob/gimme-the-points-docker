const fetch = require('node-fetch')
const {trendDate} = require('./helpers')
const config = require('../config/config.json')
const {logError, logStats} = require('./logger')

//should throw error if google trends is down/unavaliable
function checkStatus (res) {
  if (!res.ok) throw new Error ('Error fetching google trends')
  return res
}

//fetch google trends
function fetchTrends (date, geo) {
  console.log(date, geo)
  return fetch(`https://trends.google.com/trends/api/dailytrends?hl=en-GB&ed=${date}&geo=${geo}&ns=15`)
  .then(res => checkStatus(res))
  .then(res => res.text())
}

//parse google trends
function parseTrends (trendText) {
  return JSON.parse(trendText.slice(6, trendText.length)).default.trendingSearchesDays[0].trendingSearches
}

// unique set of trend queries, small random element, skip adding a trend/related
function addUniqueQueries (parsedTrends, queries) {
  parsedTrends.forEach(trend => {
    if (Math.floor(Math.random() * 2) > 0) queries.add(trend.title.query)
    addRelatedQueries(queries, trend)
  })
  return queries
}

//add related queries to the set
function addRelatedQueries (trendSet, trend) {
  trend.relatedQueries.forEach(relatedTrend => {
    if (Math.floor(Math.random() * 2) > 0) trendSet.add(relatedTrend.query)
  })
  return trendSet
}

//geo of trends to get
function trendGeo () {
  return config.geos[Math.floor((Math.random()*config.geos.length))]
}

//filter queries down to amount
function filterQueries (queriesArray, amount) {
  const queries = []
  queriesArray.map(query => {
    while (queries.length < amount) {
      queries.push(query)
      break;
    }
  })
  return queries
}

//runs above functions to get needed queries for bot
async function getQueries (amount) {
  const queries = new Set()
  while (queries.size <= amount) {
    const trends = await fetchTrends(trendDate(), trendGeo())
    await addUniqueQueries(parseTrends(trends), queries)
  }
  return filterQueries([...queries], amount)
}

module.exports = {getQueries}




