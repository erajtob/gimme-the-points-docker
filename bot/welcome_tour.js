const {randSeconds, withDelay, elementToExist, elementIsDisplayed} = require('./helpers')
const {By} = require('selenium-webdriver');


async function getWelcomeTourState() {
  let tourState = await driver.findElement(By.className('welcome-tour-avaliable-counter'))
  return await tourState.getText()
}

async function getTourLength(tourState) {
  let done, need
  if (tourState.length == 4) {
    done = tourState.slice(0,1), need = tourState.slice(2,4)
  }
  if (tourState.length == 5) {
    if (tourState[1] != '/') return done = tourState.slice(0,2), need = tourState.slice(3,5)
    done = tourState.slice(0,1), need = tourState.slice(2,5)
  }
  if (tourState.length == 6) {
    done = tourState.slice(0,2), need = tourState.slice(3,6)
  }
  return parseInt((need - done) / 25)
}


async function tourStart() {
  await elementToExist(By.className('welcome-tour-next-button'))
  await withDelay(randSeconds())
  await driver.findElement(By.className('welcome-tour-next-button')).click()
  await withDelay(randSeconds())
  await elementToExist(By.className('quiz-link'))
}


async function tourNext() {
  const nextButtons = await driver.findElements(By.className('welcome-tour-next-button'))
  for (let index = 0; index < nextButtons.length; index++) {
    const button = nextButtons[index];
    if (await button.isDisplayed()) {
      await button.click()
      break
    }
  }
  await withDelay(randSeconds())
}

async function tourAnswer() {
  const answerBtn = await driver.findElements(By.className('quiz-link'))
  for (let index = 0; index < answerBtn.length; index++) {
    const button = answerBtn[index];
    if (await button.isDisplayed()) {
      await button.click()
      break
    }
  }
}

async function tourClose() {
  await elementIsDisplayed(By.className('welcome-tour-close-link'))
  await driver.findElement(By.className('welcome-tour-close-link')).click()
}

const welcomeTourCheck = async function (url) {
  if (!/welcometour/.test(url)) return
  try {
    await elementIsDisplayed(By.className('c-subheading-5'))
    //tourTest()
    await welcomeTour2()
  } catch (error) {
    console.log(error)
    await driver.get('https://account.microsoft.com/rewards/')
  }
}

const welcomeTour2 = async function () {
  const tourState = await getWelcomeTourState()
  const tourLength = await getTourLength(tourState)
  if(tourState[0] == '0') await tourStart()
  for (let index = 0; index < tourLength; index++) {
    await tourAnswer()
    await withDelay(randSeconds())
    await tourNext()
  }
  await tourClose()
}

module.exports = {welcomeTourCheck}