const { runAll, startAt, single, dashboardOnly, mobileOnly, emailOnly, test } = require('./bot/init')
const { setAccounts } = require('./bot/helpers')
const { logStats, logError, createLogFile } = require('./bot/logger')
const { checkForUpdate, autoUpdate } = require('./bot/updater')
const cron = require('node-cron')
const accounts = setAccounts()


async function initiate (args) {
  createLogFile()
  if (args.runtype == 'single') {
    logStats(`Bot attempting to run single account: ${accounts[args.account].email}`)
    await single(accounts[args.account], args.emaillinks)

  }

  if (args.runtype == 'startat') {
    logStats(`Bot attempting to run startat account from: ${accounts[args.account].email}`)
    await startAt(accounts, args.account, args.emaillinks)
  }

  if (args.runtype == 'all') {
    logStats(`Bot attempting to run all accounts...`)
    await runAll(accounts, args.emaillinks)
  }

  if (args.runtype == 'dashboard') {
    logStats(`Bot attempting to run dashboard only for ${accounts[args.account].email}`)
    await dashboardOnly(accounts[args.account])
  }

  if (args.runtype == 'mobile') {
    logStats(`Bot attempting to run mobile only for ${accounts[args.account].email}`)
    await mobileOnly(accounts[args.account])
  }

  if (args.runtype == 'emailonly') {
    await emailOnly(accounts[args.account])
  }

  if (args.runtype == 'test') {
    await test(accounts[args.account])
  }
}

const start = async function () {
  const args = require('minimist')(process.argv.slice(2))
  //guards
  if (!args.ignoreupdates && !args.autoupdate) await checkForUpdate()
  if (!args.ignoreupdates && args.autoupdate) await autoUpdate()
  if (!args.runtype) return console.log('Please enter runtype')
  if (accounts[0].email == 'example@email.com' || accounts.length == 0) return console.log('please add your own accounts in config/accounts.json')
  if (args.runtype == 'single' || args.runtype == 'startat' || args.runtype == 'dashboard' || args.runtype == 'mobile') {
    if (typeof args.account != 'number') return console.log('Please enter an account number')
  }

  try {
    if (args.schedule) {
      let schedule = args.schedule.split(/[h,m]/)
      if (!/[0-9]/.test(schedule[1]) | !/[0-9]/.test(schedule[0])) {
        throw new Error ('Invalid schedule. Should look like this - schedule=9h23m')
      }
      console.log(`Schedule set. Will start at ${schedule[0]}:${schedule[1]} every day.`)
      cron.schedule(`${schedule[1]} ${schedule[0]} * * *`, async () => {
        await initiate(args)
      })
    }
    if (!args.schedule) {
      await initiate(args)
      process.exit()
    }
    
  } catch (error) {
    logStats('The bot has encountered an error: '+ error.name)
    logError(error)
  }
  
}

start()